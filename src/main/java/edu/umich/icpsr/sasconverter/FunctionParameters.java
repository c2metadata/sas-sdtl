package edu.umich.icpsr.sasconverter;

public class FunctionParameters
{
    public Boolean isRequired;
    public Object defaultValue;
    public String param;
    public String name;
    public String position;
    public String index_offset;
    public String type;

    public FunctionParameters(){}
    /*public FunctionParameters(String required, String param, String type)
    {
        this.required = required;
        this.param = param;
        this.type = type;
    }
    public FunctionParameters(String required, String param, String name,
                              String position, String index_offset)
    {
        this.required = required;
        this.param = param;
        this.name = name;
        this.position = position;
        this.index_offset = index_offset;
    }
    // depending on whether 3 or 5 args are given, args are expected
    // to be in the same order as their respective constructors above
    public FunctionParameters(String... args)
    {
        this.required = args[0];
        this.param = args[1];
        if(args.length == 3)
        {
            this.type = args[2];
        }
        else if(args.length == 5)
        {
            this.name = args[2];
            this.position = args[3];
            this.index_offset = args[4];
        }
    }*/
}
