/*
Copyright 2020 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019 and 2020.
*/

package edu.umich.icpsr.sasconverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FunctionNameMap
{
    public static final Map<String, Function> functions = new HashMap<>();
    public static final Map<String, String> HorizontalMap = new HashMap<>();
    public static final Map<String, String> CollapseMap = new HashMap<>();

    static
    {
        //String filepath = "/home/alecastle/sas-sdtl/src/main/resources/SDTL_Function_Library.json";
        String filepath = "/home/alecastle/sas-sdtl/src/main/resources/SDTL_Function_Library_04.json";
        try
        {
            String file = new String(Files.readAllBytes(Paths.get(filepath)));
            ObjectMapper mapper = new ObjectMapper();
            FunctionLibrary library = mapper.readValue(file, FunctionLibrary.class);
            for(Function function : library.horizontal)
            {
                for(FunctionVariant variant : function.SAS)
                {
                    HorizontalMap.put(variant.function_name, function.SDTLname);
                    functions.put(variant.function_name, function);
                }
            }
            for(Function function : library.collapse)
            {
                for(FunctionVariant variant : function.SAS)
                {
                    CollapseMap.put(variant.function_name, function.SDTLname);
                    functions.put(variant.function_name, function);
                }
            }
        }
        catch (IOException | JSONException e)
        {
            e.printStackTrace();
        }
    }
}

// POJO for the overall Library
class FunctionLibrary
{
    public List<Function> horizontal;
    public List<Function> vertical;
    public List<Function> collapse;
    public List<Function> logical;

    public FunctionLibrary(){}
}
