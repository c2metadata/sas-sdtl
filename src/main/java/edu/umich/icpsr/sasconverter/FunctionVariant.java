package edu.umich.icpsr.sasconverter;

import java.util.ArrayList;
import java.util.List;

public class FunctionVariant
{
    public String function_name;
    public String syntax;
    public String URL;
    public List<FunctionParameters> parameters;

    public FunctionVariant(){}
    /*public FunctionVariant(String function_name, String syntax, String URL)
    {
        this.function_name = function_name;
        this.syntax = syntax;
        this.URL = URL;
        this.parameters = new ArrayList<>();
    }
    // argument order is expected to be the same as the above constructor
    public FunctionVariant(String... args)
    {
        this.function_name = args[0];
        this.syntax = args[1];
        this.URL = args[2];
        this.parameters = new ArrayList<>();
    }
    public void addParameters(String required, String param, String name,
                              String position, String offset)
    {
        FunctionParameters parameters = new FunctionParameters(required, param, name, position, offset);
        this.parameters.add(parameters);
    }
    public void addParameters(String... args)
    {
        FunctionParameters parameters = new FunctionParameters(args[0], args[1], args[2], args[3], args[4]);
        this.parameters.add(parameters);
    }*/
}
