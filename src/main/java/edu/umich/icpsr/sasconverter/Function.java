package edu.umich.icpsr.sasconverter;

import java.util.List;
import java.util.ArrayList;

public class Function
{
    public String SDTLname;
    public String n_arguments;
    public String returnType;
    public String definition;
    public String notes;
    public String Pseudocode;
    public List<FunctionVariant> SPSS;
    public List<FunctionVariant> Stata;
    public List<FunctionVariant> SAS;
    public List<FunctionVariant> R;
    public List<FunctionVariant> Python;
    public List<FunctionParameters> parameters;

    public Function(){}
    /*public Function(String SDTLname, String n_arguments, String returnType,
                    String definition, String notes, String pseudocode)
    {
        this.SDTLname = SDTLname;
        this.n_arguments = n_arguments;
        this.returnType = returnType;
        this.definition = definition;
        this.notes = notes;
        this.pseudocode = pseudocode;
        this.parameters = new ArrayList<>();
        this.sas = new ArrayList<>();
    }
    // arguments are expected in the same order as the above constructor
    public Function(String... args)
    {
        this.SDTLname = args[0];
        this.n_arguments = args[1];
        this.returnType = args[2];
        this.definition = args[3];
        this.notes = args[4];
        this.pseudocode = args[5];
        this.parameters = new ArrayList<>();
        this.sas = new ArrayList<>();
    }
    public void addParameters(String param, String required, String type)
    {
        FunctionParameters parameters = new FunctionParameters(required, param, type);
        this.parameters.add(parameters);
    }
    public void addVariant(String name, String syntax, String URL)
    {
        FunctionVariant variant = new FunctionVariant(name, syntax, URL);
        this.sas.add(variant);
    }
    public void addVariantParameters(int index, String required, String param,
                                     String name, String position, String offset)
    {
        this.sas.get(index).addParameters(required, param, name, position, offset);
    }
    public void addParameters(String... args)
    {
        FunctionParameters parameters = new FunctionParameters(args[0], args[1], args[2]);
        this.parameters.add(parameters);
    }
    public void addVariant(String... args)
    {
        FunctionVariant variant = new FunctionVariant(args[0], args[1], args[2]);
        this.sas.add(variant);
    }
    public void addVariantParameters(int index, String... args)
    {
        this.sas.get(index).addParameters(args[0], args[1], args[2], args[3], args[4]);
    }*/
}
