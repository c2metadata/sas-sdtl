package edu.umich.icpsr.sasconverter;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Input
{
	public Parameters parameters;
	public Input() {}
}