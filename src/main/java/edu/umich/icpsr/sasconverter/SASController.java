/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Alexander Mueller in 2019.
*/

package edu.umich.icpsr.sasconverter;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import edu.umich.icpsr.sastosdtl.models.Program;

@Path("/hello")
public class SASController
{
	@GET
	@Path("/world")
	public Response helloWorld() 
	{
		return Response.status(200).entity("Hello World").build();
	}
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@JsonInclude(JsonInclude.Include.NON_NULL)
    public Program getProgram(Input input) throws JSONException, IOException, NoSuchAlgorithmException
    {
    	Converter converter = new Converter();
    	String sas_code = input.parameters.sas;
    	HashMap<String, List<String>> file_variables = new HashMap<>();
    	HashMap<String, List<String>> dataset_variables = new HashMap<>();
    	
    	for (DataFileDescription desc : input.parameters.data_file_descriptions) 
    	{
    		List<String> variable_list = desc.variables;
    		
    		if(file_variables.containsKey(desc.input_file_name))
            {
                throw new IOException("Error: duplicate filename in input file list. Please rename your files.");
            }

    		if(desc.input_file_name.contains("WORK."))
    		{
    			String dataset_name = desc.input_file_name.split("WORK.")[1];
    			dataset_variables.put(dataset_name, variable_list);
			}
    		else if(desc.input_file_name.contains(".sas7bdat"))
			{
				file_variables.put(desc.input_file_name, variable_list);
			}
    		else
			{
				file_variables.put(desc.input_file_name + ".sas7bdat", variable_list);
			}
    	}//System.err.println("FILE VARIABLES " + file_variables + "\nDATASET VARIABLES " + dataset_variables);
    	
    	return converter.convertSAS(sas_code, file_variables, dataset_variables);
    }
}
