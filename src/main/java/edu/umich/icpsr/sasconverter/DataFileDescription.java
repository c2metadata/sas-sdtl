package edu.umich.icpsr.sasconverter;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataFileDescription
{
	public String input_file_name;
	public String DDI_XML_file;
	public String file_name_DDI;
	public List<String> variables;
	public DataFileDescription() {}
}