package edu.umich.icpsr.sasconverter;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameters
{
	public String sas;
	public List<DataFileDescription> data_file_descriptions;
	public Parameters() {}
}