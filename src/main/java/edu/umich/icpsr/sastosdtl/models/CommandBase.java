package edu.umich.icpsr.sastosdtl.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommandBase
{
    public String command;
    public String $type;
    public List<SourceInformation> sourceInformation;
    public String messageText;

    public CommandBase()
    {
        this.sourceInformation = new ArrayList<>();
    }

    @JsonIgnore
    // this method name is capitalized as a workaround to avoid serialization
    // (apparently jackson serializes any Boolean method whose name starts with "is",
    // paying no heed to JsonIgnore and/or JsonIgnoreProperties annotations)
    public Boolean IsCommandList() {
        return false;
    }
}
