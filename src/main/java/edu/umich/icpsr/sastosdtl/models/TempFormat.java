package edu.umich.icpsr.sastosdtl.models;

import java.util.List;

// Note: this class doesn't represent a specific SDTL type;
// it is merely a placeholder for user-defined formats
public class TempFormat extends CommandBase
{
    public String name;
    public List<ValueLabel> labels;

    public TempFormat(String name, List<ValueLabel> labels)
    {
        this.$type = "TempFormat";
        this.name = name;
        this.labels = labels;
    }
}
