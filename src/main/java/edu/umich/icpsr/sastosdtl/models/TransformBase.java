/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified by Alexander Mueller in 2019.
*/

package edu.umich.icpsr.sastosdtl.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransformBase extends CommandBase
{
    public List<DataframeDescription> consumesDataframe;
    public List<DataframeDescription> producesDataframe;

    TransformBase()
    {
        this.consumesDataframe = null;
        this.producesDataframe = null;
    }
    public void consume(String name, List<String> variables)
    {
        if(this.consumesDataframe == null)
            this.consumesDataframe = new ArrayList<>();
        this.consumesDataframe.add(new DataframeDescription(name, variables));
    }

    public void produce(String name, List<String> variables)
    {
        if(this.producesDataframe == null)
            this.producesDataframe = new ArrayList<>();
        this.producesDataframe.add(new DataframeDescription(name, variables));
    }

    public void setConsumes(int index, String name, List<String> variables)
    {
        this.consumesDataframe.set(index, new DataframeDescription(name, variables));
    }

    public void setConsumes(int index, String name)
    {
        List<String> variables = this.consumesDataframe.get(index).variableInventory;//System.out.println("set variables: " + variables);
        this.consumesDataframe.set(index, new DataframeDescription(name, variables));
    }

    public void setProduces(int index, String name, List<String> variables)
    {
        this.producesDataframe.set(index, new DataframeDescription(name, variables));
    }

    public void setProduces(int index, String name)
    {
        List<String> variables = this.producesDataframe.get(index).variableInventory;
        this.producesDataframe.set(index, new DataframeDescription(name, variables));
    }

    public void setProducesToConsumes()
    {
        this.producesDataframe = new ArrayList<>();
        for(DataframeDescription description : this.consumesDataframe)
        {
            this.produce(description.dataframeName, description.variableInventory);
        }
    }

    public static void copyConsumes(TransformBase first, TransformBase second)
    {
        second.consumesDataframe = new ArrayList<>();
        for(DataframeDescription description : first.consumesDataframe)
        {
            second.consume(description.dataframeName, description.variableInventory);
        }
    }

    public static void copyProduces(TransformBase first, TransformBase second)
    {
        second.producesDataframe = new ArrayList<>();
        for(DataframeDescription description : first.producesDataframe)
        {
            second.produce(description.dataframeName, description.variableInventory);
        }
    }

    public static void produceConsumed(TransformBase first, TransformBase second)
    {
        second.producesDataframe = new ArrayList<>();
        for(DataframeDescription description : first.consumesDataframe)
        {
            second.produce(description.dataframeName, description.variableInventory);
        }
    }

    public static void consumeProduced(TransformBase first, TransformBase second)
    {
        second.consumesDataframe = new ArrayList<>();
        for(DataframeDescription description : first.producesDataframe)
        {
            second.consume(description.dataframeName, description.variableInventory);
        }
    }

}
