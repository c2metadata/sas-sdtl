package edu.umich.icpsr.sastosdtl.models;

import java.util.ArrayList;
import java.util.List;

public class DataframeDescription
{
    public String dataframeName;
    public List<String> variableInventory;
    public DataframeDescription(String name, List<String> variables)
    {
        this.dataframeName = name.contains("_activeDataframe_") ? name : name.toUpperCase();
        this.variableInventory = new ArrayList<>(variables);
    }
}
