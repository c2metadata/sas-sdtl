package edu.umich.icpsr.sastosdtl.models;

public class NoTransformOp extends InformBase
{
    public NoTransformOp()
    {
        this.$type = "NoTransformOp";
        this.command = "NoTransformOp";
    }
}
