libname data_in "D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\GSS_36797\MVP_scripts\GSS_SAS";
libname data_out "D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\GSS_36797\MVP_scripts\GSS_SAS\output";

data gss2;
  set DATA_IN.C2M_36797_0001_DATA  ;
  drop age age_rec age_comp;
run;