libname data_in "D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\GSS_36797\MVP_scripts\GSS_SAS";
libname data_out "D:\Users\altergc\Documents\ICPSR\Project development\metadata_capture\scripts\GSS_36797\MVP_scripts\GSS_SAS\output";

data gss2;
    set DATA_IN.C2M_36797_0001_DATA  ;

    ******* RECODE ***************;
    age_rec=age;
    select  ;
        when (age_rec>=15 and age_rec<30) age_rec=15;
        when (age_rec>=30 and age_rec<50) age_rec=30;
        when (age_rec>=50) age_rec=50;
        otherwise age_rec=age_rec;
    end;

    label age_rec="Age recoded by RECODE" ;
    Proc format;
        value  agereclab  15="15-29"
                          30="30-49"
                          50="50+";
    format  age_rec agereclab. ;
run;

data gss3;
   set gss2;

    ******* RENAME *****************;
    rename age_rec=age_3cat ;

    ******* COMPUTE *****************;
    age_comp=10*int(age/10);
    label age_comp="Age categorized by COMPUTE";

run;


**********  tables **************;
proc freq data=gss3;
    tables age_3cat  age_comp  ;

run;

data gss4;
    set gss3;
    ********** IF  ********************;
    if age>70 then age_comp = 70 ;
run;

proc freq data=gss4;
    tables age_comp ;
run;

********* SELECT CASES ***************;
data gss5;
    set gss4;
    if age <20 then delete;
    if age <50 ;
run;

proc freq data=gss5;
    tables  age_comp ;
run;

data gss6;
     set gss5;
    ******** SELECT VARIABLES ************;

    drop abany--absingle;
    keep age-- degree educ--age_comp;

    ******** DEFINE MISSING VALUE **********;
    ***  tab1 PARTYID1 PARTYID2 PARTYID3 , miss;
    ***  mvdecode PARTYID1-PARTYID3 , mv(4);

    if PARTYID1=4 then   PARTYID1=. ;
    if PARTYID2=4 then   PARTYID2=. ;
    if PARTYID3=4 then   PARTYID3=. ;

run;

proc freq data=gss6;
    tables PARTYID1 PARTYID2 PARTYID3 ;
run;

************ SORT VARIABLES *****************;

data gss7;
    set gss6;
    XX1=1 ;
    Xx2=2 ;
    xx3=3 ;
    xX4=4 ;
    XX5=5 ;
run;

***********************************************************************************************;
******   SAS does not have a built-in procedure for sorting variables by name ************************;
******   order _all , alpha           **********************************************************;


**** drop XX1 XX5 Xx2 *********;
data gss8;
    set gss7;
    drop XX1 Xx2 XX5 ;
run;

****** AGGREGATE ****************;
****** SAS does not have a procedure for adding aggregate variables to an existing dataset ***********;
****** SAS Proc SQL can be used for aggregating to a collapsed dataset, which can then be merged to the original dataset *********;
*** by cohort year , sort: egen cy_inc_mean=mean(income)
*** by cohort year , sort: egen cy_inc_n=count(income)
*** sum cy_inc_mean cy_inc_n

****** FLOW CONTROL -- LOOP BY VARIABLES ****************;
data gss9;
    set gss8;
    democrats=0;
    republicans=0;
    array partyidk PARTYID1 PARTYID2 PARTYID3 ;
    do k=1 to 3;
        if partyidk(k)=1 then democrats=democrats+1 ;
        if partyidk(k)=2 then republicans=republicans+1 ;
    end;
run;

proc freq data=gss9;
    tables democrats*republicans ;
run;


****** FLOW CONTROL -- LOOP BY NUMBERS *****************;
data gss10;
    set gss9;
    idealfam=.;
    do k = 0 to 9;
        if k=chldidel then idealfam=k;
    end;
run;


proc freq data=gss10;
    tables chldidel*idealfam ;
run;