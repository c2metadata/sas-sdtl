data gss2;
    set gss;
    where x;

data gss3;
    set gss (where=(x));

data gss4 (where=(x));
    set gss;

data gss5;
    set gss;
    if x;

data gss6;
    set gss;
    where x<0;

data gss7;
    set gss (where=(x<0));

data gss8 (where=(x<0));
    set gss;

data gss9;
    set gss;
    if x<0;