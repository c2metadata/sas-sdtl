proc means data=hogwarts;
    class House Role;
    output out=hogwarts_out
        mean(Age)=Mean_Age
        median(Year)=Median_Year
        max(Income)=Max_Income;