/*
Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created by Yashas Vaidya in 2018.
File modified extensively by Alexander Mueller in 2019.
*/

package SASConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import edu.umich.icpsr.sasconverter.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import javax.json.Json;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static org.junit.Assert.assertEquals;


public class ConverterTest
{
    @Test
    public void test_AppendDatasets() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing append originating from a SET command...",
            "Testing append originating from an APPEND procedure..."
        };
        runTestCase("AppendDatasets", 2, messages);
    }

    @Test
    public void test_attrib() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing format, label, and TRANSCODE attributes with both single and multi variable statments..."
        };
        runTestCase("attrib", 1, messages);
    }

    /*@Test
    public void test_Collapse() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing collapse with default statistics and no by groups...",
            "Testing collapse with explicitly named statistics and no by groups...",
            // TODO: test case with no VAR statement, no explicitly named analysis variables, and multiple by groups
            "Testing collapse with named statistics, one by group, and explicit analysis variables...",
            "Testing collapse with by groups listed in class statement and default types...",
            "Testing collapse with class statement and types according to ways statement...",
            "Testing collapse with only overall aggregates (from TYPES statement with empty list)...",
            "Testing collapse with explicitly defined 1-way and 2-way aggregates...",
            "Testing collapse with a combination of TYPES and WAYS statements..."
        };
        runTestCase("Collapse", 8, messages);
    }*/

    @Test
    public void test_Comment() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single line comment starting with * and ending with ; ...",
            "Testing single line comment starting with /* and ending with */ ...",
            "Testing multi line comment starting with * and ending with ; ...",
            "Testing multi line comment starting with /* and ending with */ ..."
        };
        runTestCase("Comment", 4, messages);
    }

    @Test
    public void test_Compute() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing compute with one parenthetical, one function call, and two arithmetic operations...",
            "Same as above, but testing a different function call...",
            "Testing compute base case with just arithmetic...",
            "Testing multi-layered sequence of function calls, parentheticals, and arithmetic operations..."
        };
        runTestCase("Compute", 4, messages);
    }

    @Test
    public void test_dataset_options() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing lone DROP=, KEEP=, and RENAME= dataset options on the output...",
            "Testing multiple output datasets, each with their own dataset option...",
            "Testing multiple dataset options on the same output dataset...",
            "Testing the order in which drops, keeps, and renames are applied with statements vs. dataset options...",
            "Same as above, but a more rigorous test that includes both input and output dataset options..."
        };
        runTestCase("dataset_options", 5, messages);
    }

    @Test
    public void test_DropVariables() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single variable deletion...",
            "Testing multi variable deletion...",
            "Testing variable range deletion..."
        };
        runTestCase("DropVariables", 3, messages);
    }

    @Test
    public void test_IfRows() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing simple IF/THEN/ELSE syntax..."
        };
        runTestCase("IfRows", 1, messages);
    }

    @Test
    public void test_KeepCases() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing the base cases for all SAS statements or dataset options that result in KeepCases...",
            "Testing more complicated WHERE expression logic, including operators specific to WHERE expressions..."//,
            //"Testing a maximally convoluted scenario for reordering of computes, drops, keeps, renames, and wheres..."
        };
        runTestCase("KeepCases", 2, messages);
    }

    @Test
    public void test_KeepVariables() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single variable keep...",
            "Testing multi variable keep...",
            "Testing variable range keep..."
        };
        runTestCase("KeepVariables", 3, messages);
    }

    @Test
    public void test_LoopOverList() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing loop over variables..."
            // TODO: test case for loop over values
        };
        runTestCase("LoopOverList", 1, messages);
    }

    // not testing this until it's done
    /*@Test
    public void test_merge_modify_update() throws IOException, NoSuchAlgorithmException, JSONException
    {
        HashMap<String, List<String>> dataset_variables = new HashMap<>();
        dataset_variables.put("master", Arrays.asList("ID", "Name", "Winnings", "Age"));
        dataset_variables.put("transaction", Arrays.asList("ID", "Name", "Winnings", "Reliability"));
        // TODO: add message string and call to runTestCase()
    }*/

    @Test
    public void test_MergeDatasets() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing MERGE with a by group...",
            "Testing the different types of merging (e.g. Sequential vs. SASmatchMerge)..."
        };
        runTestCase("MergeDatasets", 2, messages);
    }

    @Test
    public void test_SetMissingValues() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single missing value...",
            "Testing multiple missing values..."
        };
        runTestCase("SetMissingValues", 2, messages);
    }

    @Test
    public void test_Rename() throws JSONException, IOException, NoSuchAlgorithmException
    {
        String[] messages =
        {
            "Testing single rename...",
            "Testing multiple renames..."
        };
        runTestCase("Rename", 2, messages);
    }

    // not testing this until there's something to test
    /*@Test
    public void test_SetValueLabels() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing the setting of labels for values..."
        };
        //runTestCase("SetValueLabels", 1, messages);
    }*/

    @Test
    public void test_SetDisplayFormat() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing character and numeric display formats with multiple variables, including default formats...",
            "Testing format dissociation..."
        };
        runTestCase("SetDisplayFormat", 2, messages);
    }

    @Test
    public void test_SetVariableLabel() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing single variable label...",
            "Testing multi variable label..."
        };
        runTestCase("SetVariableLabel", 2, messages);
    }

    /*@Test
    public void integration_test() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages =
        {
            "Testing all types of comments amongst compute, drop, keep, label, and rename...",
            "Testing unquoted labels, comments in strings, and multiline assignment with * ...",
            "Testing value labels, compute, if, keep/drop cases/variables, both types of loops...",
            "Testing macros, append, collapse, merge..."
        };
        runTestCase("integration", 4, messages);
    }*/

    @Test
    public void test_SortCases() throws IOException, NoSuchAlgorithmException, JSONException
    {
        String[] messages = { "Testing descending and ascending sort..." };
        runTestCase("SortCases", 1, messages);
    }

    private void runTestCase(String file_pattern, int num_test_cases, String[] messages) throws IOException, NoSuchAlgorithmException, JSONException
    {
        String input_path = "/home/alecastle/sas-sdtl/src/test/java/SASConverter/JSON_input/";
        String sas_path = "/home/alecastle/sas-sdtl/src/test/java/SASConverter/SAS_files/";
        String sdtl_path = "/home/alecastle/sas-sdtl/src/test/java/SASConverter/SDTL_files/";
        boolean production = false;

        for(int x=0; x < num_test_cases; x++)
        {
            String input_file = input_path + file_pattern + "_" + x + ".json";
            String sdtl_file = sdtl_path + file_pattern + "_" + x + "_SDTL.json";

            System.out.print(messages[x]);
            ObjectMapper mapper = new ObjectMapper();
            Input input = mapper.readValue(new FileInputStream(input_file), Input.class);

            String sas_code = input.parameters.sas;
            HashMap<String, List<String>> file_variables = new HashMap<>();
            HashMap<String, List<String>> dataset_variables = new HashMap<>();
            for(DataFileDescription description : input.parameters.data_file_descriptions)
            {
                if(description.input_file_name.contains("WORK."))
                {
                    String dataset_name = description.input_file_name.split("WORK.")[1];
                    dataset_variables.put(dataset_name, description.variables);
                }
                else
                {
                    file_variables.put(description.input_file_name, description.variables);
                }
            }
            JSONArray program_output = Test_Command(sas_code, file_variables, dataset_variables); // run SAS through Converter
            JSONArray correct_output = Text_To_JSON(sdtl_file); // get correct SDTL from test case
            assertEquals(correct_output.toString(4), program_output.toString(4)); // print assertion error if the two don't match
            System.out.println("PASSED");
        }
    }

    private void makeProductionTest(String input_file, String sas_file, HashMap<String, List<String>> file_variables) throws IOException
    {
        Input input = new Input();
        input.parameters = new Parameters();
        input.parameters.data_file_descriptions = new ArrayList<>();
        for(String key : file_variables.keySet())
        {
            DataFileDescription description = new DataFileDescription();
            description.input_file_name = key;
            description.variables = file_variables.get(key);
            input.parameters.data_file_descriptions.add(description);
        }
        input.parameters.sas = Read_file(sas_file);

        File file = new File(input_file);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(file, input);
    }

    private static String runShellCommandWithOutput(String command) throws IOException
    {
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder output = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) { output.append(line); }
        return output.toString();
    }

    private JSONArray Test_input(String input_file) throws IOException
    {
        ProcessBuilder maven_builder = new ProcessBuilder("mvn", "clean", "tomcat:run");
        final Process maven_process = maven_builder.start();
        BufferedReader maven_reader = new BufferedReader(new InputStreamReader(maven_process.getErrorStream()));
        String maven_line;
        OuterLoop:while((maven_line = maven_reader.readLine()) != null)
        {//System.out.println(maven_line);
            // triggered when the build has reached the appropriate stage to test
            if(maven_line.contains("Starting Coyote HTTP/1.1 on http-8080"))
            {
                ProcessBuilder curl_builder = new ProcessBuilder("curl", "-X", "POST",
                        "http://localhost:8080/sasconverter/hello/post", "-d", "@" + input_file,
                        "-H", "Content-Type: application/json");
                final Process curl_process = curl_builder.start();
                BufferedReader curl_reader = new BufferedReader(new InputStreamReader(curl_process.getInputStream()));
                BufferedWriter curl_writer = new BufferedWriter(new FileWriter(new File("output.json")));
                String curl_line;
                while((curl_line = curl_reader.readLine()) != null)
                {//System.out.println(curl_line);
                    if(curl_line.contains("{\"iD\":"))
                    {//System.out.println(curl_line);
                        curl_writer.write(curl_line);
                        curl_writer.close();
                        break OuterLoop;
                    }
                }
            }
        }
        Runtime.getRuntime().exec("kill $(lsof -t -i :8080)");  // kill the maven process
        JSONObject output = text_to_JSON_Object("output.json");
        return output.getJSONArray("commands");
    }

    private JSONArray Test_Command(String sas_code, HashMap<String, List<String>> file_variables, HashMap<String, List<String>> dataset_variables) throws JSONException, IOException, NoSuchAlgorithmException {

        // Read in SAS file
        //String input_file_string = Read_file(input_file);
        //System.out.println(input_file_string);

        // Instantiate converter object and send in SAS
        Converter converter = new Converter();
        String all_text = converter.convertSAS(sas_code, file_variables, dataset_variables).toString();

        // Turn String to JSON
        JSONObject obj = new JSONObject(all_text);

        // Obtain only "Commands" object
        JSONArray commands = obj.getJSONArray("commands");
        //System.out.println(commands.toString(4));

        // Remove SourceInformation object
        //removeSourceInformation(commands);

        return sort_JSON_Array(commands);
    }

    private JSONArray Text_To_JSON(String input_file) throws JSONException, IOException
    {
        String input_file_string = Read_file(input_file);

        // Convert to JSON
        return sort_JSON_Array(new JSONArray(input_file_string));
    }

    private JSONObject text_to_JSON_Object(String input_file) throws JSONException, IOException
    {
        String input_file_string = Read_file(input_file);
        JSONObject object = new JSONObject(input_file_string);
        removeNullProperties(object);
        return object;
    }

    private String Read_file(String input_file) throws IOException
    {
        return new String(Files.readAllBytes(Paths.get(input_file)));
    }

    private JSONObject sort_JSON_Object(JSONObject input)
    {
        Set<String> input_keys = input.keySet();
        TreeSet<String> output_keys = new TreeSet<>();
        output_keys.addAll(input_keys);
        JSONObject output = new JSONObject();
        for(String key : output_keys)
        {
            output.put(key, input.get(key));
        }
        return output;
    }

    private JSONArray sort_JSON_Array(JSONArray input)
    {
        for(int x=0; x < input.length(); x++)
        {
            input.put(x, sort_JSON_Object((JSONObject) input.get(x)));
        }
        return input;
    }

    private void removeNullProperties(JSONObject input) throws JSONException
    {
        ArrayList<String> null_outer_keys = new ArrayList<>();
        input.keySet().forEach(key ->
        {
            Object value = input.get(key);
            if(value == null || key.equals("commandList"))
            {
                null_outer_keys.add(key);
            }
            else if(value instanceof JSONObject)
            {
                removeNullProperties((JSONObject) value);
            }
            else if(value instanceof JSONArray)
            {
                JSONArray value_array = (JSONArray) value;
                if(value_array.isEmpty())
                    null_outer_keys.add(key);
                else
                {
                    for(int x=0; x < value_array.length(); x++)
                    {
                        Object element = value_array.get(x);
                        if(element instanceof JSONObject)
                            removeNullProperties((JSONObject) element);
                    }
                }
            }
        });
        for(String key : null_outer_keys)
        {
            input.remove(key);
        }
    }

    private void removeSourceInformation(JSONArray input) throws JSONException
    {
        for(int x=0; x < input.length(); x++)
        {
            input.getJSONObject(x).remove("sourceInformation");
            if(input.getJSONObject(x).has("command"))
            {
                if(input.getJSONObject(x).getString("command").equals("LoopOverList"))
                {
                    removeSourceInformation(input.getJSONObject(x).getJSONArray("commands"));
                }
                if(input.getJSONObject(x).getString("command").equals("IfRows"))
                {
                    removeSourceInformation(input.getJSONObject(x).getJSONArray("thenCommands"));
                    if(input.getJSONObject(x).has("elseCommands"))
                    {
                        removeSourceInformation(input.getJSONObject(x).getJSONArray("elseCommands"));
                    }
                }
            }
        }
    }

}